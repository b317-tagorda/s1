SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName= "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > "3000";

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";


SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";


SELECT firstName, lastName, FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";


SELECT ee.firstName, ee.lastName, office.city 
	FROM employees ee JOIN offices office ON ee.officeCode = office.officeCode
	WHERE ee.officeCode = 5;



SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";


SELECT products.productName, customers.customerName
	FROM products
		JOIN orderdetails ON products.productCode = orderdetails.productCode
		JOIN orders ON orderdetails.orderNumber = orders.orderNumber
		JOIN customers ON orders.customerNumber = customers.customerNumber
	WHERE customers.customerName = "Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country AS office_country
	FROM employees
		JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
		JOIN offices ON employees.officeCode = offices.officeCode
	WHERE customers.country = offices.country;



SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < "1000";






SELECT customerName FROM customers WHERE phone LIKE "%+81%";
