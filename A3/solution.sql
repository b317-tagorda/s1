INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("jjuandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

INSERT INTO posts (users_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-21 01:00:00");
INSERT INTO posts (users_id, title, content, datetime_posted) VALUES (2, "Second Code", "Hello Earth!", "2021-01-21 02:00:00");
INSERT INTO posts (users_id, title, content, datetime_posted) VALUES (3, "Third Code", "Welcome to Mars!", "2021-01-21 03:00:00");
INSERT INTO posts (users_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye Bye Solar System!", "2021-01-21 04:00:00");
INSERT INTO posts (users_id, title, content, datetime_posted) VALUES (5, "Fifth Code", "Hello Neighbors", "2021-01-21 05:00:00");


SELECT content FROM posts WHERE users_id = "1";

SELECT email, datetime_created from users

UPDATE posts SET content = "Hello to the people of Earth!" WHERE id = "8";


DELETE FROM users WHERE email = "johndoe@gmail.com";